package com.example.roomlp2122.BD;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Personne.class},version=1)
public abstract class PersonRoomDatabase extends RoomDatabase {
    //un getter abstrait pour chaque DAO qu'on souhaite utiliser
    public abstract PersonDao personDao();

    private static PersonRoomDatabase INSTANCE;

    static PersonRoomDatabase getDatabase(final Context context){
        if (INSTANCE==null){
            synchronized (PersonRoomDatabase.class){
                if (INSTANCE==null){
                    INSTANCE = Room.databaseBuilder(
                            context.getApplicationContext(),
                            PersonRoomDatabase.class,
                            "personnes_database").build();

                }
            }
        }
        return INSTANCE;
    }


}
