package com.example.roomlp2122.BD;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface PersonDao {
    @Insert
    void insert(Personne personne);

    @Query("DELETE From person_table")
    void deleteAll();

    @Query("SELECT count(*) From person_table")
    Integer nbPersons();

    @Query("SELECT count(*) From person_table")
    LiveData<Integer> nbPersonsLD();

    @Query("Select * From person_table order by nom ASC")
    LiveData<List<Personne>> getAllPersonsLD();
}
