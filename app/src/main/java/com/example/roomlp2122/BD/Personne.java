package com.example.roomlp2122.BD;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName="person_table")
public class Personne {
    @PrimaryKey(autoGenerate=true)
    private int id;

    private String nom;
    private String prenom;

    public Personne(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
}
