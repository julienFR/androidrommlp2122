package com.example.roomlp2122.BD;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import java.util.List;
import java.util.concurrent.Executors;

public class PersonRepository {
    private PersonDao personDao;
    private LiveData<Integer> nbPersonnes;
    private LiveData<List<Personne>> allPersonnes;

    public PersonRepository(Application application){
        PersonRoomDatabase db = PersonRoomDatabase.getDatabase(application);
        personDao = db.personDao();
        nbPersonnes = personDao.nbPersonsLD();
        allPersonnes = personDao.getAllPersonsLD();
    }

    public LiveData<Integer> getNbPersonnes() {
        return nbPersonnes;
    }

    public LiveData<List<Personne>> getAllPersonnes() {
        return allPersonnes;
    }

    public void insert(Personne p){
        new InsertAsync(personDao).execute(p);
    }

    private static class InsertAsync extends AsyncTask<Personne,Void,Void>{
        private PersonDao personDao;

        InsertAsync(PersonDao personDao){
            this.personDao = personDao;
        }

        @Override
        protected Void doInBackground(Personne... p){
            personDao.insert(p[0]);
            return null;
        }
    }

    public Integer getNbPerson(){
        try {
            return new GetNbPersAsync(personDao).execute().get();
        }catch (Exception e){
            Log.d("MesLogs","pb getNbPerson");
        }
        return null;
    }

    private static class GetNbPersAsync extends AsyncTask<Void,Void,Integer>{
        private PersonDao personDao;

        GetNbPersAsync(PersonDao personDao){
            this.personDao = personDao;
        }

        @Override
        protected Integer doInBackground(Void... voids){
            return personDao.nbPersons();
        }
    }

    public void deleteAll(){
        Executors.newSingleThreadScheduledExecutor().execute(
                new Runnable() {
                    @Override
                    public void run() {
                        personDao.deleteAll();
                    }
                }
        );
    }
}
